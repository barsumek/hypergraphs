from unittest.mock import MagicMock
from hypergraph import Hypergraph
from productions.p2 import p2
import unittest
import uuid


def mock_graph_to_p2():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    x1 = 0
    x2 = 1
    y1 = 0
    y2 = 1

    vertices_positions = ((x1, y2), (x2, y2), (x1, y1), (x2, y1))
    vertices = []

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)
    graph.add_edge(frozenset(vertices), {"label": "I", "break": 1})

    return graph


class P1Test(unittest.TestCase):
    def setUp(self):
        graph = mock_graph_to_p2()
        for e in graph.edges:
            if len(e) == 4:
                hyper_edge = e
                break
        self.G = p2(graph, hyper_edge)

    def test_nodes_size(self):
        # 4 old nodes + 1 new node
        self.assertEqual(len(self.G.nodes), 5)
        # 5 nodes + 4 nodes for edges between new and old nodes + 2 node for F1 + 2 node for F2
        self.assertEqual(self.G.graph.number_of_nodes(), 13)

    def test_edges_size(self):
        # 4 edges between new node and old nodes + 4 single edges for new node
        self.assertEqual(len(self.G.edges), 8)
        # 4*2 edges between old nodes and new node + 4 single edges for new node
        self.assertEqual(self.G.graph.number_of_edges(), 12)

    def node_check(self, node_num, geom, r, g ,b):
        self.assertTrue(node_num in self.G.nodes)
        self.assertTrue(self.G.graph.has_node(node_num))
        attributes = self.G.get_node_attributes('attr_dict')[node_num]
        self.assertEqual(attributes['geom'], geom)
        self.assertEqual(attributes['R'], r)
        self.assertEqual(attributes['G'], g)
        self.assertEqual(attributes['B'], b)

    def test_node_one(self):
        self.node_check(1, (0, 1), 0, 0, 0)
        
    def test_node_two(self):
        self.node_check(2, (1, 1), 0, 0, 0)

    def test_node_three(self):
        self.node_check(3, (0, 0), 0, 0, 0)

    def test_node_four(self):
        self.node_check(4, (1, 0), 0, 0, 0)

    def test_node_five(self):
        self.node_check(5, (0.5, 0.5), 0, 0, 0)

    def edge_check(self, node_1, node_2):
        edge_node = frozenset([node_1, node_2])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_2, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'I')
        self.assertEqual(attributes['break'], 0)

    def test_edges(self):
        self.edge_check(1, 5)
        self.edge_check(2, 5)
        self.edge_check(3, 5)
        self.edge_check(4, 5)

    def test_single_edge(self):
        virtual_vert = [6, 7]
        for vert in virtual_vert:
            single_edge = frozenset([5, vert])
            self.assertTrue(single_edge in self.G.edges)
            attributes = self.G.get_edge_attributes('attr_dict')[single_edge]
            self.assertEqual(attributes['label'], 'F1')
        virtual_vert = [8, 9]
        for vert in virtual_vert:
            single_edge = frozenset([5, vert])
            self.assertTrue(single_edge in self.G.edges)
            attributes = self.G.get_edge_attributes('attr_dict')[single_edge]
            self.assertEqual(attributes['label'], 'F2')


if __name__ == '__main__':
    unittest.main()
