from unittest.mock import MagicMock
from hypergraph import Hypergraph
from productions.p4 import p4
import unittest
import uuid

x2 = -1
x1 = 0
x3 = 1
y2 = -1
y3 = 0
y1 = 1

def mock_graph_to_p4():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()

    vertices_positions = ((x1,y2), (x3,y3), (x1,y1), (x2,y3))
    vertices = []

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)
        
    graph.add_edge(frozenset([vertices[0], vertices[1]]), {"label": "I", "break": 0})
    graph.add_edge(frozenset([vertices[1], vertices[2]]), {"label": "I", "break": 0})
    graph.add_edge(frozenset([vertices[2], vertices[3]]), {"label": "I", "break": 0})
    graph.add_edge(frozenset([vertices[3], vertices[0]]), {"label": "I", "break": 0})
    graph.add_edge(frozenset([vertices[0], vertices[2]]), {"label": "F1", "break": 0})
    graph.add_edge(frozenset([vertices[1]]), {"label": "F2", "break": 0})
    graph.add_edge(frozenset([vertices[3]]), {"label": "F2", "break": 0})

    return graph, vertices


class P4Test(unittest.TestCase):
    def setUp(self):
        graph, vertices = mock_graph_to_p4()
        self.G = p4(graph, *vertices)

    def test_nodes_size(self):
        # 4 old nodes + 1 new node
        self.assertEqual(len(self.G.nodes), 5)
        # 5 nodes + 4 for I + 2 for F1 + 2 for F2
        self.assertEqual(self.G.graph.number_of_nodes(), 13)

    def test_edges_size(self):
        # 7 old edges + 1 new F1 edge
        self.assertEqual(len(self.G.edges), 8)
        # 12 old edges + 8 new to new node
        self.assertEqual(self.G.graph.number_of_edges(), 20)

    def node_check(self, node_num, geom, r, g ,b):
        self.assertTrue(node_num in self.G.nodes)
        self.assertTrue(self.G.graph.has_node(node_num))
        attributes = self.G.get_node_attributes('attr_dict')[node_num]
        self.assertEqual(attributes['geom'], geom)
        self.assertEqual(attributes['R'], r)
        self.assertEqual(attributes['G'], g)
        self.assertEqual(attributes['B'], b)

    def test_node_one(self):
        self.node_check(1, (x1, y2), 0, 0, 0)
        
    def test_node_two(self):
        self.node_check(2, (x3, y3), 0, 0, 0)

    def test_node_three(self):
        self.node_check(3, (x1, y1), 0, 0, 0)

    def test_node_four(self):
        self.node_check(4, (x2, y3), 0, 0, 0)

    def test_node_five(self):
        self.node_check(5, (x1, y3), 0, 0, 0)

    def edge_check(self, edge, label):
        self.assertTrue(edge in self.G.edges)
        attributes = self.G.get_edge_attributes('attr_dict')[edge]
        self.assertEqual(attributes['label'], label)

    def test_edges(self):
        self.edge_check(frozenset([1, 2, 5]), "I")
        self.edge_check(frozenset([2, 3, 5]), "I")
        self.edge_check(frozenset([3, 4, 5]), "I")
        self.edge_check(frozenset([4, 1, 5]), "I")
        self.edge_check(frozenset([1, 5]), "F1")
        self.edge_check(frozenset([3, 5]), "F1")
        self.edge_check(frozenset([2, 5]), "F2")
        self.edge_check(frozenset([4, 5]), "F2")


if __name__ == '__main__':
    unittest.main()
