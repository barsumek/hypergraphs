from unittest.mock import MagicMock
from hypergraph import Hypergraph
from productions.p5 import p5
import unittest
import uuid


def mock_graph_to_p5():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    x1 = 0
    x2 = 1
    y1 = 0
    y2 = 1

    vertices_positions = ((x1, y2), (x2, y2), (x1, y1), (x2, y1))
    vertices = []

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)
    graph.add_edge(frozenset(vertices), {"label": "I", "break": 0})

    return graph


class P5Test(unittest.TestCase):
    def setUp(self):
        graph = mock_graph_to_p5()
        for e in graph.edges:
            if len(e) == 4:
                hyper_edge = e
                break
        self.G = p5(graph, hyper_edge)

    def test_nodes_size(self):
        # 4 old nodes + 1 new node
        self.assertEqual(len(self.G.nodes), 4)

    def test_break_attr(self):
        edge_node = frozenset([1, 2, 3, 4])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(1, edge_node))
        self.assertTrue(self.G.graph.has_edge(2, edge_node))
        self.assertTrue(self.G.graph.has_edge(3, edge_node))
        self.assertTrue(self.G.graph.has_edge(4, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'I')
        self.assertEqual(attributes['break'], 1)


if __name__ == '__main__':
    unittest.main()
