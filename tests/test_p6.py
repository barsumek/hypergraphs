from unittest.mock import MagicMock
from hypergraph import Hypergraph
from productions.p6 import p6
import unittest
import uuid


def mock_massive_to_p6():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    x1 = 0
    x2 = 1
    x3 = 3
    y1 = 2
    y2 = 1
    y3 = 0

    p1 = (x1, y1)
    p2 = (x1, y2)
    p3 = (x3, y1)
    p4 = (x3, y3)

    p5 = (x2, y1)
    p6 = (x2, y3)

    p7 = (x2, y2)
    p8 = (-1, 2)

    p9 = (-1, 4)
    p10 = (1, 4)

    vertices_positions = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)

    graph.add_edge(frozenset((2, 5)), {"label": "I", "break": 1})
    graph.add_edge(frozenset((5, 6)), {"label": "F1"})
    graph.add_edge(frozenset((5, 6, 3, 4)), {"label": "I", "break": 0})

    graph.add_edge(frozenset((5, 8)), {"label": "F1"})
    graph.add_edge(frozenset((5, 8, 9, 10)), {"label": "I", "break": 0})

    return graph, (1, 2, 3, 4)

def mock_graph_to_p6():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    x1 = 0
    x2 = 1
    x3 = 3
    y1 = 2
    y2 = 1
    y3 = 0

    p1 = (x1, y1)
    p2 = (x1, y2)
    p3 = (x3, y1)
    p4 = (x3, y3)

    p5 = (x2, y1)
    p6 = (x2, y3)

    vertices_positions = (p1, p2, p3, p4, p5, p6)

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)

    graph.add_edge(frozenset((2, 5)), {"label": "I", "break": 1})
    graph.add_edge(frozenset((5, 6)), {"label": "F1"})
    graph.add_edge(frozenset((5, 6, 3, 4)), {"label": "I", "break": 0})

    return graph, (1, 2, 3, 4)

def mock_graph_to_p6_horizontal():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    x1 = 0
    x2 = 1
    x3 = 2
    y1 = 0
    y2 = 1
    y3 = 3

    p1 = (x1, y1)
    p2 = (x2, y1)
    p3 = (x1, y3)
    p4 = (x3, y3)

    p5 = (x1, y2)
    p6 = (x3, y2)

    vertices_positions = (p1, p2, p3, p4, p5, p6)

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)

    graph.add_edge(frozenset((1, 2, 5)), {"label": "I", "break": 1})
    graph.add_edge(frozenset((5, 6)), {"label": "F1"})
    graph.add_edge(frozenset((5, 6, 3, 4)), {"label": "I", "break": 0})

    return graph, (1, 2, 3, 4)


class P6Test(unittest.TestCase):
    def setUp(self):
        graph, vertices = mock_graph_to_p6()
        self.G = p6(graph, *vertices)

    def test_nodes_size(self):
        # 4 old nodes + 1 new node
        self.assertEqual(len(self.G.nodes), 6)

    def test_break_attr(self):
        edge_node = frozenset([5, 6, 3, 4])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(5, edge_node))
        self.assertTrue(self.G.graph.has_edge(6, edge_node))
        self.assertTrue(self.G.graph.has_edge(3, edge_node))
        self.assertTrue(self.G.graph.has_edge(4, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'I')
        self.assertEqual(attributes['break'], 1)

    def test_break_attr_horizontal(self):
        graph, vertices = mock_graph_to_p6_horizontal()
        G = p6(graph, *vertices)
        edge_node = frozenset([5, 6, 3, 4])
        self.assertTrue(edge_node in G.edges)
        self.assertTrue(G.graph.has_node(edge_node))
        self.assertTrue(G.graph.has_edge(5, edge_node))
        self.assertTrue(G.graph.has_edge(6, edge_node))
        self.assertTrue(G.graph.has_edge(3, edge_node))
        self.assertTrue(G.graph.has_edge(4, edge_node))
        attributes = G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'I')
        self.assertEqual(attributes['break'], 1)


if __name__ == '__main__':
    unittest.main()
