import unittest
import uuid
from unittest.mock import MagicMock

from hypergraph import Hypergraph
from productions.p3 import p3


def mock_graph_to_p3_horizontal():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    y1 = y2 = 1
    y3 = 0
    x1 = 0
    x3 = 1
    x2 = 2
    xf1 = 1
    yf1 = 0.5

    vertices_positions = [(x1, y1), (x2, y2), (x3, y3)]
    vertices = []

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)

    vertex_id = uuid.uuid4()
    vertex_attr = {"geom": (xf1, yf1)}
    graph.add_node(vertex_id, vertex_attr)

    graph.add_edge(frozenset([vertices[2], vertex_id]), {"label": "F1", 'dir': 'N'})
    graph.add_edge(frozenset([vertices[0], vertices[2]]), {"label": "I"})
    graph.add_edge(frozenset([vertices[1], vertices[2]]), {"label": "I"})
    graph.add_edge(frozenset([vertices[0], vertices[1]]), {"label": "B"})

    return graph, frozenset([vertices[0], vertices[1]]), vertices[2]


def mock_graph_to_p3_vertical():
    uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
    graph = Hypergraph()
    y1 = 2
    y2 = 0
    y3 = 1
    x1 = x2 = 0
    x3 = 1
    xf2 = 0.5
    yf2 = 1

    vertices_positions = [(x1, y1), (x2, y2), (x3, y3)]
    vertices = []

    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)

    vertex_id = uuid.uuid4()
    vertex_attr = {"geom": (xf2, yf2)}
    graph.add_node(vertex_id, vertex_attr)

    graph.add_edge(frozenset([vertices[2], vertex_id]), {"label": "F2", 'dir': 'W'})
    graph.add_edge(frozenset([vertices[0], vertices[2]]), {"label": "I"})
    graph.add_edge(frozenset([vertices[1], vertices[2]]), {"label": "I"})
    graph.add_edge(frozenset([vertices[0], vertices[1]]), {"label": "B"})

    return graph, frozenset([vertices[0], vertices[1]]), vertices[2]


class P1Test(unittest.TestCase):
    def setUp(self):
        graph, edge, v3 = mock_graph_to_p3_horizontal()
        graph_v, edge_v, v3_v = mock_graph_to_p3_vertical()
        self.G = p3(graph, edge, v3)
        self.G_v = p3(graph_v, edge_v, v3_v)

    def test_nodes_size(self):
        # 3 old nodes + 1 new node
        self.assertEqual(len(self.G.nodes), 4)
        # 4 nodes + 4 nodes for edges between new and old nodes + 1 node for F1
        self.assertEqual(self.G.graph.number_of_nodes(), 9)

    def test_edges_size(self):
        self.assertEqual(len(self.G.edges), 5)
        self.assertEqual(self.G.graph.number_of_edges(), 12)

    def node_check(self, node_num, geom, r, g ,b):
        self.assertTrue(node_num in self.G.nodes)
        self.assertTrue(self.G.graph.has_node(node_num))
        attributes = self.G.get_node_attributes('attr_dict')[node_num]
        self.assertEqual(attributes['geom'], geom)
        self.assertEqual(attributes['R'], r)
        self.assertEqual(attributes['G'], g)
        self.assertEqual(attributes['B'], b)

    def test_node_one(self):
        self.node_check(1, (0, 1), 0, 0, 0)
        
    def test_node_two(self):
        self.node_check(2, (2, 1), 0, 0, 0)

    def test_node_three(self):
        self.node_check(3, (1, 0), 0, 0, 0)

    def test_node_four(self):
        self.node_check(5, (1, 1), 0, 0, 0)

    def hyper_edge_check(self, node_1, node_2, node_3):
        edge_node = frozenset([node_1, node_2, node_3])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_2, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_3, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'I')

    def test_hyper_edges(self):
        self.hyper_edge_check(1, 3, 5)
        self.hyper_edge_check(2, 3, 5)

    def normal_edge_check(self, node_1, node_2, label):
        edge_node = frozenset([node_1, node_2])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_2, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], label)

    def edge_check(self, node_1, node_2, label, direction):
        edge_node = frozenset([node_1, node_2])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_2, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], label)
        self.assertEqual(attributes['dir'], direction)

    def edge_check_vertical(self, node_1, node_2, label, direction):
        edge_node = frozenset([node_1, node_2])
        self.assertTrue(edge_node in self.G_v.edges)
        self.assertTrue(self.G_v.graph.has_node(edge_node))
        self.assertTrue(self.G_v.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G_v.graph.has_edge(node_2, edge_node))
        attributes = self.G_v.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], label)
        self.assertEqual(attributes['dir'], direction)

    def test_edges(self):
        self.normal_edge_check(1, 5, 'B')
        self.normal_edge_check(2, 5, 'B')
        self.edge_check(3, 5, 'F1', 'N')
        self.edge_check_vertical(3, 6, 'F2', 'W')


if __name__ == '__main__':
    unittest.main()
