from productions.p1 import p1
import unittest
from unittest.mock import MagicMock
import uuid

class P1Test(unittest.TestCase):
    def setUp(self):
        uuid.uuid4 = MagicMock(side_effect=[x for x in range(1, 1000)])
        self.G = p1()

    def test_nodes_size(self):
        self.assertEqual(len(self.G.nodes), 4)
        # 4 nodes + 4 nodes for edges between two nodes + 1 node for hyperedge between all nodes
        self.assertEqual(self.G.graph.number_of_nodes(), 9)

    def test_edges_size(self):
        # 4 edges + hyperedge
        self.assertEqual(len(self.G.edges), 5)
        # 4*2 edges between two nodes + 4 as hyperedge between all 4 nodes
        self.assertEqual(self.G.graph.number_of_edges(), 12)

    def node_check(self, node_num, geom, r, g ,b):
        self.assertTrue(node_num in self.G.nodes)
        self.assertTrue(self.G.graph.has_node(node_num))
        attributes = self.G.get_node_attributes('attr_dict')[node_num]
        self.assertEqual(attributes['geom'], geom)
        self.assertEqual(attributes['R'], r)
        self.assertEqual(attributes['G'], g)
        self.assertEqual(attributes['B'], b)

    def test_node_one(self):
        self.node_check(1, (0,1), 0, 0, 0)
        
    def test_node_two(self):
        self.node_check(2, (1,1), 0, 0, 0)

    def test_node_three(self):
        self.node_check(3, (1,0), 0, 0, 0)

    def test_node_four(self):
        self.node_check(4, (0,0), 0, 0, 0)

    def edge_check(self, node_1, node_2):
        edge_node = frozenset([node_1, node_2])
        self.assertTrue(edge_node in self.G.edges)
        self.assertTrue(self.G.graph.has_node(edge_node))
        self.assertTrue(self.G.graph.has_edge(node_1, edge_node))
        self.assertTrue(self.G.graph.has_edge(node_2, edge_node))
        attributes = self.G.get_edge_attributes('attr_dict')[edge_node]
        self.assertEqual(attributes['label'], 'B')

    def test_edges(self):
        self.edge_check(1, 2)
        self.edge_check(2, 3)
        self.edge_check(3, 4)
        self.edge_check(1, 4)

    def test_hyperedge(self):
        hyper_edge = frozenset([1, 2, 3, 4])
        self.assertTrue(hyper_edge in self.G.edges)
        self.assertTrue(self.G.graph.has_node(hyper_edge))
        self.assertTrue(self.G.graph.has_edge(1, hyper_edge))
        self.assertTrue(self.G.graph.has_edge(2, hyper_edge))
        self.assertTrue(self.G.graph.has_edge(3, hyper_edge))
        self.assertTrue(self.G.graph.has_edge(4, hyper_edge))
        attributes = self.G.get_edge_attributes('attr_dict')[hyper_edge]
        self.assertEqual(attributes['label'], 'I')
        self.assertEqual(attributes['break'], 0)


if __name__ == '__main__':
    unittest.main()
