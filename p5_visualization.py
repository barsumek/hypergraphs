from productions.p1 import *
from productions.p5 import p5
import matplotlib.pyplot as plt
import networkx as nx

graph = p1()
hyper_edge = None
for e in graph.edges:
    if len(e) == 4:
        hyper_edge = e
        break


def create_edge_label(d):
    label = d['attr_dict']['label']
    if 'break' in d['attr_dict']:
        label += ", break=" + str(d['attr_dict']['break'])
    return label


if hyper_edge:
    p5(graph, hyper_edge)
    G = graph.graph
    vertices = [n for n in G.nodes if not isinstance(n, frozenset)]
    edge_vertices = [n for n in G.nodes if isinstance(n, frozenset)]

    node_labels = dict((n, (d['attr_dict']['geom'])) for n, d in G.nodes(data=True) if n in vertices)
    print(node_labels)
    edge_labels = dict((n, create_edge_label(d)) for n, d in G.nodes(data=True) if n in edge_vertices)
    labels = {**node_labels, **edge_labels}

    nodes_pos = node_labels
    edges_nodes_pos = {}

    for edge_vertex in edge_vertices:
        nodes_geoms = [nodes_pos[vertex_id] for vertex_id in edge_vertex]
        x = sum(list(map(lambda geom: geom[0], nodes_geoms))) / len(nodes_geoms)
        y = sum(list(map(lambda geom: geom[1], nodes_geoms))) / len(nodes_geoms)
        edges_nodes_pos[edge_vertex] = (x, y)

    pos = {**nodes_pos, **edges_nodes_pos}

    nx.draw_networkx_nodes(G, pos, nodelist=vertices, node_shape='o')
    nx.draw_networkx_nodes(G, pos, nodelist=edge_vertices, node_shape='s')
    nx.draw_networkx_edges(G, pos)
    nx.draw_networkx_labels(G, pos, labels=labels)
    plt.show()