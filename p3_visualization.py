import matplotlib.pyplot as plt
import networkx as nx

from productions.p3 import p3
from tests.test_p3 import mock_graph_to_p3_horizontal


def visualize_p3():
    graph, edge, v3 = mock_graph_to_p3_horizontal()

    p3(graph, edge, v3)
    G = graph.graph
    vertices = [n for n in G.nodes if not isinstance(n, frozenset)]
    edge_vertices = [n for n in G.nodes if isinstance(n, frozenset)]

    # vertices = [n for n,d in G.nodes(data=True) if n in vertices and not d['attr_dict']['geom'] == 'virtual']

    node_labels = dict((n, d['attr_dict']['geom']) for n, d in G.nodes(data=True) if n in vertices)
    edge_labels = dict((n, d['attr_dict']['label']) for n, d in G.nodes(data=True) if n in edge_vertices)
    labels = {**node_labels, **edge_labels}

    nodes_pos = node_labels
    edges_nodes_pos = {}

    for edge_vertex in edge_vertices:
        nodes_geoms = [nodes_pos[vertex_id] for vertex_id in edge_vertex]
        x = sum(list(map(lambda geom: geom[0], nodes_geoms))) / len(nodes_geoms)
        y = sum(list(map(lambda geom: geom[1], nodes_geoms))) / len(nodes_geoms)
        edges_nodes_pos[edge_vertex] = (x, y)

    pos = {**nodes_pos, **edges_nodes_pos}

    nx.draw_networkx_nodes(G, pos, nodelist=vertices, node_shape='o')
    nx.draw_networkx_nodes(G, pos, nodelist=edge_vertices, node_shape='s')
    nx.draw_networkx_edges(G, pos)
    nx.draw_networkx_labels(G, pos, labels=labels)
    plt.show()


if __name__ == '__main__':
    visualize_p3()
