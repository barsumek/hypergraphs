from productions.p2 import p2
import matplotlib.pyplot as plt
import networkx as nx

from tests.test_p2 import mock_graph_to_p2


def visualize_p2():
    graph = mock_graph_to_p2()
    for e in graph.edges:
        if len(e) == 4:
            hyper_edge = e
            break

    if hyper_edge:
        p2(graph, hyper_edge)
        G = graph.graph
        vertices = [n for n in G.nodes if not isinstance(n, frozenset)]
        edge_vertices = [n for n in G.nodes if isinstance(n, frozenset)]

        node_labels = dict((n, d['attr_dict']['geom']) for n, d in G.nodes(data=True) if n in vertices)
        edge_labels = dict((n, d['attr_dict']['label']) for n, d in G.nodes(data=True) if n in edge_vertices)
        labels = {**node_labels, **edge_labels}

        nodes_pos = node_labels
        edges_nodes_pos = {}

        for edge_vertex in edge_vertices:
            virt_vert = None
            cont = False
            for vertex_id in edge_vertex:
                if vertex_id not in G.nodes:
                    virt_vert = vertex_id
                    cont = True
                    break
            if cont:
                tmp_list = (list(edge_vertex))
                tmp_list.remove(virt_vert)
                direction = graph.get_node_attributes('attr_dict')[edge_vertex]['dir']
                pos = graph.get_node_attributes('attr_dict')[tmp_list[0]]['geom']
                if direction == 'N':
                    edges_nodes_pos[edge_vertex] = (pos[0], pos[1]+pos[1]/2)
                elif direction == 'S':
                    edges_nodes_pos[edge_vertex] = (pos[0], pos[1]-pos[1]/2)
                elif direction == 'E':
                    edges_nodes_pos[edge_vertex] = (pos[0]+pos[0]/2, pos[1])
                elif direction == 'W':
                    edges_nodes_pos[edge_vertex] = (pos[0]-pos[0]/2, pos[1])
                continue
            nodes_geoms = [nodes_pos[vertex_id] for vertex_id in edge_vertex]
            x = sum(list(map(lambda geom: geom[0], nodes_geoms))) / len(nodes_geoms)
            y = sum(list(map(lambda geom: geom[1], nodes_geoms))) / len(nodes_geoms)
            edges_nodes_pos[edge_vertex] = (x, y)

        pos = {**nodes_pos, **edges_nodes_pos}

        nx.draw_networkx_nodes(G, pos, nodelist=vertices, node_shape='o')
        nx.draw_networkx_nodes(G, pos, nodelist=edge_vertices, node_shape='s')
        nx.draw_networkx_edges(G, pos)
        nx.draw_networkx_labels(G, pos, labels=labels)
        plt.show()

if __name__ == '__main__':
    visualize_p2()
