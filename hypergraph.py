import networkx as nx


class Hypergraph:
    def __init__(self):
        self.graph = nx.Graph()
        self.nodes = []
        self.edges = []

    def add_node(self, n, attr=None):
        """
        Add a single node n and update node attributes.

        :param n: Node id.
        :param attr: Dictionary of node attributes.
        :return:
        """

        if n not in self.nodes:
            # add the node
            self.nodes.append(n)

        self.graph.add_node(n, attr_dict=attr)

    def remove_node(self, n):
        try:
            self.nodes.remove(n)
            self.graph.remove_node(n)
        except (KeyError, ValueError):
            print("Node does not exist")

        # remove all hyperedges containing node n
        self.edges = list(filter(lambda edge: n not in edge, self.edges))

    def get_node_attributes(self, attribute):
        """
        :param n:
        :param attribute:
        :return: Dictionary of attributes keyed by node
        """
        return nx.get_node_attributes(self.graph, attribute)

    def add_edge(self, vertices, attr=None):
        """
        Add hyperedge between the vertices from vertices set.
        If the edge already exists the function only changes its attributes.

        :param vertices: Set of adjacent vertices
        :param attr: Dictionary of edge attributes
        :return:
        """

        # we assume that there do not exist any parallel hyperedges
        if vertices not in self.edges:
            self.edges.append(vertices)

        # Create a node in self.graph representing the edge
        self.graph.add_node(vertices, attr_dict=attr)

        # check if vertices from list exist, if not - create them
        # (without any attribute

        for v in vertices:
            if v not in self.nodes:
                self.add_node(v)

            self.graph.add_edge(v, vertices)

    def remove_edge(self, vertices):
        try:
            self.edges.remove(vertices)
            self.graph.remove_node(vertices)
        except KeyError:
            print("Node does not exist")

    def get_edge_attributes(self, attribute):
        """
        :param n:
        :param attribute:
        :return: Dictionary of attributes keyed by edge
        """
        return nx.get_node_attributes(self.graph, attribute)

    def remove_virtual_node(self, n):
        try:
            if nx.get_node_attributes(self.graph, 'attr_dict')[n]['geom'] == 'virtual':
                self.nodes.remove(n)
                self.graph.remove_node(n)
        except (KeyError, ValueError):
            print("Node does not exist")
