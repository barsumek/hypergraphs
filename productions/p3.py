import uuid
from typing import FrozenSet

from hypergraph import Hypergraph


def p3(graph: Hypergraph, edgex: FrozenSet[uuid.UUID], v3: uuid.UUID) -> Hypergraph:
    """
    Breaks B edge and creates new vertex containing following attributes:
    - geom = ((x1+x2)/2, (y1+y2)/2)
    - R, G, B = 0.
    Also creates/updates edges to newly created vertex.
    """

    # check if the given edge is valid
    assert_edge(graph, edgex)

    # find hyper edges between v1 and v3 also v2 and v3
    edge_13, edge_23, edge_f = None, None, None
    new_edge = list(edgex)
    v1, v2 = new_edge[0], new_edge[1]

    attr = graph.get_node_attributes('attr_dict')
    v1_pos = attr[v1]['geom']
    v2_pos = attr[v2]['geom']
    v3_pos = attr[v3]['geom']

    # dir = label = None

    # E, W, N, S
    if v1_pos[1] == v2_pos[1] and v3_pos[1] < v1_pos[1]:
        direction = 'N'
        label = 'F1'
    elif v1_pos[1] == v2_pos[1] and v3_pos[1] > v1_pos[1]:
        direction = 'S'
        label = 'F1'
    elif v1_pos[0] == v2_pos[0] and v3_pos[0] < v1_pos[0]:
        direction = 'E'
        label = 'F2'
    elif v1_pos[0] == v2_pos[0] and v3_pos[0] > v1_pos[0]:
        direction = 'W'
        label = 'F2'
    else:
        raise ValueError('Given edge has wrong geom attributes')

    edges_attr = graph.get_edge_attributes('attr_dict')

    for edge in graph.edges:
        if v1 in edge and v3 in edge:
            edge_13 = list(edge)
        elif v2 in edge and v3 in edge:
            edge_23 = list(edge)
        elif v3 in edge and len(edge) == 2 and edges_attr[edge]['label'] == label \
                and edges_attr[edge]['dir'] == direction:
            edge_f = list(edge)

    if edge_13 is None or edge_23 is None or edge_f is None:
        raise ValueError('Given hyper graph does not contains all necessary edges')

    # get x1, x2, y1, y2
    x12, y12 = [], []
    for v in edgex:
        pos = graph.get_node_attributes("attr_dict")[v]['geom']
        x12.append(pos[0])
        y12.append(pos[1])

    # init new vertex
    new_vertex_id = uuid.uuid4()
    new_vertex_pos = ((x12[0] + x12[1]) / 2, (y12[0] + y12[1]) / 2)
    new_vertex_attr = {"geom": new_vertex_pos}
    for N in ("R", "G", "B"):
        new_vertex_attr[N] = 0

    graph.add_node(new_vertex_id, new_vertex_attr)

    # remove old edges
    graph.remove_edge(edgex)
    graph.remove_edge(frozenset(edge_13))
    graph.remove_edge(frozenset(edge_23))
    graph.remove_edge(frozenset(edge_f))
    graph.remove_node([v for v in edge_f if v != v3][0])

    graph.add_edge(frozenset([v1, new_vertex_id]), {"label": "B"})
    graph.add_edge(frozenset([new_vertex_id, v2]), {"label": "B"})
    graph.add_edge(frozenset(edge_13 + [new_vertex_id]), {"label": "I", "break": 0})
    graph.add_edge(frozenset(edge_23 + [new_vertex_id]), {"label": "I", "break": 0})
    graph.add_edge(frozenset([v3, new_vertex_id]), {"label": label, 'dir': direction})

    return graph


def assert_edge(graph, edge):
    if not len(edge) == 2:
        raise ValueError('Given edge does not contains 2 vertices')
    edge_label = graph.get_node_attributes("attr_dict")[edge]['label']
    if not edge_label or edge_label != 'B':
        raise ValueError('Given edge has invalid label attribute')
