import uuid


def p2(graph, hyper_edge):
    #check if the given hyper edge is vaild
    assert_hyper_edge(graph, hyper_edge)
    # get x1, x2, y1, y2
    x12 = set()
    y12 = set()
    for v in hyper_edge:
        pos = graph.get_node_attributes("attr_dict")[v]['geom']
        x12.add(pos[0])
        y12.add(pos[1])
    x12 = list(x12)
    y12 = list(y12)

    #init new vertex
    new_vertex_id = uuid.uuid4()
    new_vertex_pos = ((x12[0] + x12[1])/2, (y12[0] + y12[1])/2)
    new_vertex_attr = {"geom": new_vertex_pos}
    for N in ("R", "G", "B"):
        new_vertex_attr[N] = 0

    graph.add_node(new_vertex_id, new_vertex_attr)
    #create edges with breaks = 0
    edges = []
    for v in hyper_edge:
        edge = (frozenset([v, new_vertex_id]), {"label": "I", "break": 0})
        edges.append(edge)

    #remove hyper_edge
    graph.remove_edge(hyper_edge)

    #add new edges
    edges_ids = []
    for edge in edges:
        graph.add_edge(edge[0], edge[1])

    #add edges containing only the new vertex (one-vertex edge)
    new_vertex_attr_virtual = new_vertex_attr.copy()
    new_vertex_attr_virtual["geom"] = "virtual"
    virtuals = []
    for i in range(0,4):
        virtuals.append(uuid.uuid4())
    for v in virtuals:
        graph.add_node(v, new_vertex_attr_virtual)
    graph.add_edge(frozenset([new_vertex_id, virtuals[0]]), {"label": "F1", "dir": "N"})
    graph.add_edge(frozenset([new_vertex_id, virtuals[1]]), {"label": "F1", "dir": "S"})
    graph.add_edge(frozenset([new_vertex_id, virtuals[2]]), {"label": "F2", "dir": "W"})
    graph.add_edge(frozenset([new_vertex_id, virtuals[3]]), {"label": "F2", "dir": "E"})
    for v in virtuals:
        graph.remove_virtual_node(v)
    return graph


def assert_hyper_edge(graph, edge):
    if not len(edge) == 4:
        raise ValueError('Given edge does not contains 4 vertices')
    attributes = graph.get_node_attributes('attr_dict')[edge]
    if not attributes['label'] or attributes['label'] != 'I':
        raise ValueError('Given edge has invalid label')
    edge_break = graph.get_node_attributes("attr_dict")[edge]['break']
    if not edge_break or edge_break != 1:
        raise ValueError('Given edge has invalid break attribute')


