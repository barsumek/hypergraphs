def p5(graph, hyper_edge):
    # check if the given hyper edge is vaild
    validate_hyper_edge(graph, hyper_edge)
    graph.get_node_attributes("attr_dict")[hyper_edge]['break'] = 1
    return graph


def validate_hyper_edge(graph, edge):
    if not len(edge) == 4:
        raise ValueError('Given edge does not contains 4 vertices')
    edge_break = graph.get_node_attributes("attr_dict")[edge]['break']
    if edge_break is None or edge_break != 0:
        raise ValueError('Given edge has break attribute equal to 1')
