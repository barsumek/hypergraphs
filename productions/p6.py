

def p6(graph, v1, v2, v3, v4):

    v1_pos = graph.get_node_attributes("attr_dict")[v1]['geom']
    v2_pos = graph.get_node_attributes("attr_dict")[v2]['geom']
    v3_pos = graph.get_node_attributes("attr_dict")[v3]['geom']
    v4_pos = graph.get_node_attributes("attr_dict")[v4]['geom']

    v5 = None
    v6 = None
    edge_5634 = None
    horizontal = None
    for edge in graph.edges:
        if len(edge) == 4 and v3 in edge and v4 in edge:
            other_v = [v for v in edge if v not in (v3, v4)]
            v5 = None
            for v in other_v:
                v_pos = graph.get_node_attributes("attr_dict")[v]['geom']
                if v_pos[0] == v1_pos[0] + ((v3_pos[0] - v1_pos[0]) / 3) and v3_pos[0] - v1_pos[0] != 0:
                    v5 = v
                    horizontal = True
                    break
                if v_pos[1] == v1_pos[1] + ((v3_pos[1] - v1_pos[1]) / 3) and v3_pos[1] - v1_pos[1] != 0:
                    v5 = v
                    horizontal = False
                    break
            if v5 is not None:
                v5_pos = graph.get_node_attributes("attr_dict")[v5]['geom']
                v6 = None
                maybe_v6 = [v for v in edge if v not in (v3, v4, v5)][0]
                maybe_v6_pos = graph.get_node_attributes("attr_dict")[maybe_v6]['geom']
                if horizontal:
                    if maybe_v6_pos[0] == v5_pos[0] and maybe_v6_pos[1] == v4_pos[1]:
                        v6 = maybe_v6
                else:
                    if maybe_v6_pos[0] == v4_pos[0] and maybe_v6_pos[1] == v5_pos[1]:
                        v6 = maybe_v6

                if v6 is not None:
                    edge_attrs = graph.get_node_attributes("attr_dict")[edge]
                    if edge_attrs['label'] == "I" and edge_attrs['break'] == 0:
                        edge_5634 = edge
                    else:
                        raise ValueError("Graph does not have appropriate edge I")

    if v5 is None or v6 is None:
        raise ValueError("Graph does not contain necessary vertices")

    edge_56 = None
    edge_125 = None
    for edge in graph.edges:
        if (len(edge) == 3 and v1 in edge and v2 in edge and v5 in edge) or (len(edge) == 2 and v2 in edge and (v5 in edge or v6 in edge)):
            edge_attrs = graph.get_node_attributes("attr_dict")[edge]
            if edge_attrs['label'] == "I" and edge_attrs['break'] == 1:
                edge_125 = edge
        if len(edge) == 2 and v5 in edge and v6 in edge:
            if graph.get_node_attributes("attr_dict")[edge]['label'] == "F1":
                edge_56 = edge

    if edge_56 is None or edge_125 is None:
        raise ValueError("Graph does not have necessary edges")

    graph.get_node_attributes("attr_dict")[edge_5634]['break'] = 1

    return graph
