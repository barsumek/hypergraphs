import uuid


def p4(graph, v5, v6, v7, v8):
    # find hyper edges between v5-v6, v6-v7, v7-v8, v8-v5, v5-v7 and v6, v8
    edge_56, edge_67, edge_78, edge_85, edge_57, edge6, edge8 = None, None, None, None, None, None, None
    for edge in graph.edges:
        if v5 in edge and v6 in edge:
            edge_56 = list(edge)
        elif v6 in edge and v7 in edge:
            edge_67 = list(edge)
        elif v7 in edge and v8 in edge:
            edge_78 = list(edge)
        elif v8 in edge and v5 in edge:
            edge_85 = list(edge)
        elif v5 in edge and v7 in edge:
            edge_57 = list(edge)
        elif v6 in edge and len(edge) == 1:
            edge6 = list(edge)
        elif v8 in edge and len(edge) == 1:
            edge8 = list(edge)

    if None in (edge_56, edge_67, edge_78, edge_85, edge_57, edge6, edge8):
        raise ValueError('Given hyper graph does not contains all necessary edges')

    if graph.get_edge_attributes("attr_dict")[frozenset(edge_56)]['label'] != "I" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge_67)]['label'] != "I" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge_78)]['label'] != "I" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge_85)]['label'] != "I" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge_57)]['label'] != "F1" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge6)]['label'] != "F2" or \
            graph.get_edge_attributes("attr_dict")[frozenset(edge8)]['label'] != "F2":
        raise ValueError('Given edge has wrong label')

    # get x1, x2, x3, y1, y2, y3
    x, y = [], []
    pos5 = graph.get_node_attributes("attr_dict")[v5]['geom']
    pos6 = graph.get_node_attributes("attr_dict")[v6]['geom']
    pos7 = graph.get_node_attributes("attr_dict")[v7]['geom']
    pos8 = graph.get_node_attributes("attr_dict")[v8]['geom']
    if pos5[0] != pos7[0]:
        raise ValueError('Given hyper graph does not have proper geometry')
    if pos6[1] != pos8[1]:
        raise ValueError('Given hyper graph does not have proper geometry')
    x.append(pos5[0])
    x.append(pos8[0])
    x.append(pos6[0])
    y.append(pos7[1])
    y.append(pos5[1])
    y.append(pos6[1])

    # init new vertex
    new_vertex_id = uuid.uuid4()

    new_vertex_pos = (x[0], y[2])
    new_vertex_attr = {"geom": new_vertex_pos}
    for N in ("R", "G", "B"):
        new_vertex_attr[N] = 0

    graph.add_node(new_vertex_id, new_vertex_attr)

    # remove old edges
    graph.remove_edge(frozenset(edge_56))
    graph.remove_edge(frozenset(edge_67))
    graph.remove_edge(frozenset(edge_78))
    graph.remove_edge(frozenset(edge_85))
    graph.remove_edge(frozenset(edge_57))
    graph.remove_edge(frozenset(edge6))
    graph.remove_edge(frozenset(edge8))

    graph.add_edge(frozenset(edge_56 + [new_vertex_id]), {"label": "I"})
    graph.add_edge(frozenset(edge_67 + [new_vertex_id]), {"label": "I"})
    graph.add_edge(frozenset(edge_78 + [new_vertex_id]), {"label": "I"})
    graph.add_edge(frozenset(edge_85 + [new_vertex_id]), {"label": "I"})
    graph.add_edge(frozenset([v5, new_vertex_id]), {"label": "F1"})
    graph.add_edge(frozenset([v7, new_vertex_id]), {"label": "F1"})
    graph.add_edge(frozenset([v6, new_vertex_id]), {"label": "F2"})
    graph.add_edge(frozenset([v8, new_vertex_id]), {"label": "F2"})

    return graph
