import uuid
from hypergraph import Hypergraph


def p1():
    """
    Creates a new hypergraph. Its vertices contain following attributes:
    - geom = (x,y)
    - R, G, B = 0.
    The hyperedge contains attribute break = 0.
    """
    graph = Hypergraph()

    # vertex 3 from task instruction is vertex 4 for easier graph creation
    vertices_positions = ((0, 1), (1, 1), (1, 0), (0, 0))
    vertices = []

    # generate vertices
    for vertex_position in vertices_positions:
        vertex_id = uuid.uuid4()
        vertex_attr = {"geom": vertex_position}
        for N in ("R", "G", "B"):
            vertex_attr[N] = 0

        graph.add_node(vertex_id, vertex_attr)
        vertices.append(vertex_id)

    # generate edges
    # generate an edge between every two consecutive vertices
    for i in range(0, len(vertices)):
        adjacent_vertices = frozenset((vertices[i], vertices[(i+1) % (len(vertices))]))
        graph.add_edge(adjacent_vertices, {"label": "B"})

    # add the hyperedge "I"
    graph.add_edge(frozenset(vertices), {"label": "I", "break": 0})

    return graph
